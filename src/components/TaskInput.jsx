import './TaskInput.css'

export default function TaskInput() {
    return <input type='text' placeholder='Add here your task' className='input'></input>
}